
var luaTest = '\
local entity = "belt" \n\
\n\
local surface = game.player.surface \n\
local count = 0 \n\
for c in surface.get_chunks() do \n\
	for key, ent in pairs(surface.find_entities_filtered({area={{c.x * 32, c.y * 32}, {c.x * 32 + 32, c.y * 32 + 32}}, force= game.player.force})) do \n\
		if string.find(ent.name,entity) then \n\
			count = count + 1 \n\
		end \n\
	end \n\
end \n\
game.player.print(count)\
';

$(function(){
	var editor = ace.edit("editor");
		editor.setTheme("ace/theme/monokai");
		editor.getSession().setMode("ace/mode/lua");
		editor.getSession().setUseWrapMode(true);
		editor.setOption("showPrintMargin", false)
		editor.insert(luaTest);
		// http://stackoverflow.com/questions/31011721/adding-to-ace-editor-wise-autocomplete-list-user-defined-functions-and-variable
});

/*var process = require('child_process');
var Rcon = require('simple-rcon');
var psTree = require('ps-tree');

// http://stackoverflow.com/questions/18831783/calling-a-server-side-function-from-client-sidehtml-button-onclick-in-node-js
// http://stackoverflow.com/questions/35995273/how-to-run-html-file-using-node-js
// https://github.com/electron/electron-quick-start

// TODO
		// - electron app
						// - launch server with html
		// - install virtual box to get server-init working to get server log files

var client = new Rcon({
	host: '127.0.0.1',
	port: '34197',
	password: 'test'
});

function startServer(){
		process.exec('start cmd.exe /K node factorio-server.js', function(error, stdout, stderr){
				console.log('stdout: ', stdout);
				console.log('stderr: ', stderr);
				if (error !== null) {
						console.log('exec error: ', error);
				};
		});

		setTimeout( function(){ startRconClient(); }, 2000);
};

function startRconClient(){
		client.connect();

		// // client.close();

		client.on('authenticated', function() {
				console.log('Authenticated!');

				sendCommand('/evolution');
				sendCommand('/players');
				sendCommand('/silent-command print(game.server_save())');
				sendCommand('/silent-command print(get_player_online_count())');
		})

		client.on('connected', function() {
			console.log('Connected!');
		})

		client.on('disconnected', function() {
			console.log('Disconnected!');
		});
}

function sendCommand( pCommand ){
		client.exec(pCommand, function( pCallback ){
				console.log( pCommand +':\n\n', pCallback, '\n\n===============');
		});
};

startServer();*/