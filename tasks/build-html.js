var gulp = require('gulp');

//maybe also add https://github.com/jonschlinkert/gulp-htmlmin

gulp.task('build-html', function () {
	return gulp.src( [ 'src/frontend/**/*.html', '!src/frontend/**/*.tpl.html' ] )
		.pipe(gulp.dest('./build/frontend'));
});
