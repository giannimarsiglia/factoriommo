var gulp = require('gulp');

gulp.task('build-images', function () {
	return gulp.src( [ 'src/frontend/**/*.{svg,jpg,jpeg,png,gif}' ] )
		.pipe(gulp.dest('./build/frontend'));
});