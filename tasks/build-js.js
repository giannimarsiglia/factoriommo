var gulp       = require('gulp');
var gulpif     = require('gulp-if');
var concat     = require('gulp-concat');
var uglify 	   = require('gulp-uglify');
var browserify = require('gulp-browserify' );

gulp.task('build-js', function(){
	return gulp.src( 'src/frontend/**/*.js')
		.pipe(concat('app.js'))
		// .pipe(gulpif( !_debug , browserify({
		.pipe( browserify({
			paths: ['../node_modules','src/frontend/**/*.js'],
			insertGlobals : false,
			debug : false
		}))
		.pipe( uglify() )
		.pipe(gulp.dest('./build/frontend'));
});