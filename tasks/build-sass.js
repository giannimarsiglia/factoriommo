var gulp      = require('gulp');
var gulpif    = require('gulp-if');
var sass      = require('gulp-sass');
var concatCss = require('gulp-concat-css');
var uglifycss = require('gulp-uglifycss');

gulp.task('build-sass', function(){
	return gulp.src( 'src/scss/frontend/**/*.scss' )
		.pipe(sass({
	        includePaths: [
	            './node_modules/compass-mixins/lib'
	        ],
	        outputStyle: 'compressed'
	    }).on('error', sass.logError))
		.pipe(concatCss("styles.css"))
		// .pipe(gulpif(!_debug, uglifycss()))
		.pipe(uglifycss())
		.pipe(gulp.dest( './build/frontend' ));
});