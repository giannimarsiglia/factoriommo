var gulp = require('gulp');
var runSequence = require('run-sequence');

gulp.task('build', function(){
	runSequence('build-sass','build-js', 'build-html', 'build-void','build-images');
});