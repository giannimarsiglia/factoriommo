var gulp        = require('gulp');
var browserSync = require('browser-sync');

gulp.task('serve', ['build'], function(done) {
	browserSync.init({
		open: "external",
		port: 9000,

		server: {
			baseDir: [ './build/frontend' ],
			middleware: [ function (req, res, next) {
				res.setHeader('Access-Control-Allow-Origin', '*');
				var last = req.url.split('/').pop();
				if( last.indexOf('.') < 0 || last === '' )
				{
				  req.url = '/index.html';
				}
				next();
			} ]
		}
	}, done);
});
