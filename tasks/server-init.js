var gulp = require('gulp');

//  General options:
//   -h [ --help ]                      display help
//   --version                          show version information
//   -v [ --verbose ]                   enable verbose logging
//   -c [ --config ] PATH               config file to use
//   --no-log-rotation                  don't rotate log file
//   --mod-directory PATH               Mod directory to use

// Running options:
//   -s [ --map2scenario ] arg          map to scenario conversion
//   -m [ --scenario2map ] arg          scenario to map conversion
//   --apply-update arg                 immediately apply update package
//   --create FILE                      create a new map
//   --map-gen-settings FILE            Map generation settings for use with
//                                      --create. See data/map-gen-settings.exampl
//                                      e.json
//   --start-server FILE                start a multiplayer server
//   --start-server-load-scenario FILE  start a multiplayer server and load the
//                                      specified scenario
//   --start-server-load-latest         start a multiplayer server and load the
//                                      latest available save
//   --until-tick TICK                  run a save until given map tick
//   --mp-connect ADDRESS               start factorio and connect to address
//   --load-game FILE                   start Factorio and load a game in
//                                      singleplayer
//   --benchmark FILE                   load save and run benchmark
//   --benchmark-ticks N (=1.000)       number of ticks for benchmarking. Default
//                                      is 1000
//   --force-opengl                     use OpenGL for rendering
//   --force-d3d                        use Direct3D for rendering
//   --fullscreen BOOL                  start game in windowed mode (saved to
//                                      configuration)
//   --max-texture-size N               maximal size of texture that the game can
//                                      use (saved to configuration). Should be
//                                      power of two greater than 2048
//   --graphics-quality arg             accepted values: normal, low, very-low
//   --video-memory-usage arg           accepted values: all, high, medium, low
//   --gfx-safe-mode                    resets some graphics settings to values
//                                      that should work on most configurations

// Server options:
//   --port N                           network port to use
//   --bind ADDRESS[:PORT]              IP address (and optionally port) to bind
//                                      to
//   --rcon-port N                      Port to use for RCON
//   --rcon-password PASSWORD           Password for RCON
//   --server-settings FILE             Path to file with server settings. See
//

// var options = {
// 	factorioPath: '"S:/Program Files (x86)/Steam/steamapps/common/Factorio/bin/x64/factorio.exe"',
// 	port: 34197,
// 	password: 'test',
// 	savegame: ' ./server/rconTMp.zip'
// };

// var bootString = options.factorioPath + ' -v --start-server' + options.savegame + ' --rcon-port ' + options.port + ' --rcon-password ' + options.password +  ' --server-settings ./server/server-settings.json';


gulp.task('server-init', function (cb) {
	// exec(bootString, function (err, stdout, stderr) {
	// 	console.log(stdout);
	// 	console.log(stderr);
	// 	cb(err);
	// });
	// var isWin = /^win/.test(process.platform);
	var execString;

	switch(process.platform) {
		case 'win32':
			execString = 'start cmd.exe /K node factorio-server.js'
			break;
		case 'linux':
			execString = 'start cmd.exe /K node factorio-server.js'
			break;
	}

	exec(execString, function(error, stdout, stderr){
		console.log('stdout: ', stdout);
		console.log('stderr: ', stderr);
		if (error !== null) {
			console.log('exec error: ', error);
		};
	});
  // http://stackoverflow.com/questions/10522532/stop-node-js-program-from-command-line
});