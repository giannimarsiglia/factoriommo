var gulp = require('gulp');
var browserSync     = require('browser-sync');

gulp.task('watch', ['serve'], function(){
	gulp.watch( 'src/frontend/scss/**/*.scss',['build-sass', browserSync.reload]).on('change', reportChange);
	gulp.watch( 'src/frontend/js/**/*.js',['build-js', browserSync.reload]).on('change', reportChange);
	gulp.watch( 'src/frontend/index.html',['build-html', browserSync.reload]).on('change', reportChange);
	gulp.watch( 'src/frontend/void/**/*.*',['build-void', browserSync.reload]).on('change', reportChange);
	gulp.watch( 'src/frontend/**/*.{svg,jpg,jpeg,png,gif}',['build-images', browserSync.reload]).on('change', reportChange);
});

function reportChange(pEvent){
	console.log('File ' + pEvent.locations + ' was ' + pEvent.type + ', running tasks...');
}